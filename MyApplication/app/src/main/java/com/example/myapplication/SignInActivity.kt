package com.example.myapplication

import android.content.Intent
import android.os.Bundle
import android.util.Log.d
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_log_in.*

class SignInActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_log_in)
        init()
    }
    private fun init(){
        auth = Firebase.auth
        loginButton.setOnClickListener {
            signIn()
        }
    }
    private fun signIn(){
        val email:String = loginEmail.text.toString()
        val password = loginPassword.text.toString()
        if(email.isNotEmpty() && password.isNotEmpty()){
            progressBarLogin.visibility = View.VISIBLE
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    progressBarLogin.visibility = View.GONE
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        d("signIn", "signInWithEmail:success")
                        val user = auth.currentUser
                        val intent = Intent(this,MainActivity2::class.java)
                        startActivity(intent)
                        Toast.makeText(this, "", Toast.LENGTH_SHORT).show()
                    } else {
                        // If sign in fails, display a message to the user.
                        d("signIn", "signInWithEmail:failure", task.exception)
                        Toast.makeText(baseContext, "User Authentication Failed:"+task.exception?.message,Toast.LENGTH_LONG).show()
                    }

                }


        }else{Toast.makeText(this,"Please fill all fields",Toast.LENGTH_SHORT).show()}
    }
}
