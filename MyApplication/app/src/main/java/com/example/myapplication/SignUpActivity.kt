package com.example.myapplication

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.util.Patterns
import android.view.View
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_authentication.*
import kotlinx.android.synthetic.main.activity_authentication.signUpButton
import kotlinx.android.synthetic.main.activity_sign_up2.*

class SignUpActivity : AppCompatActivity() {
    private lateinit var auth: FirebaseAuth


    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser

    }

    private fun init(){
        auth = Firebase.auth
        signUpButton.setOnClickListener {
            signUp()


        }
    }

    private fun signUp(auth: Any) {
        val email:String = email.text.toString()
        val password:String = password.text.toString()
        val repeatPassword:String = repeatPassword.text.toString()

        if (email.isNotEmpty() && password.isNotEmpty() && repeatPassword.isNotEmpty()){
            if (password==repeatPassword){
                if (Patterns.EMAIL_ADDRESS.matcher(email).matches()){
                    progressBar.visibility = View.VISIBLE
                    auth.createUserWithEmailAndPassword(email,password)
                        .addOnCompleteListener(this) { task ->
                            progressBar.visibility = View.GONE
                            if (task.isSuccessful) {
                                // Sign in success, update UI with the signed-in user's information
                                d("signUp", "createUserWithEmail:success")
                                val user = auth.currentUser


                                val intent = Intent(this,MainActivity::class.java)
                                startActivity(intent)

                            } else {
                                // If sign in fails, display a message to the user.
                                d("signUp", "createUserWithEmail:failure", task.exception)
                                Toast.makeText(baseContext, "Authentication failed.", Toast.LENGTH_SHORT).show()
                            }
                        }
                    Toast.makeText(this,"Sign Up was Successful!",Toast.LENGTH_SHORT).show()
                }else{Toast.makeText(this,"Email format is not Correct!",Toast.LENGTH_SHORT).show()}

            }else{Toast.makeText(this,"Please Type  Same Passwords!",Toast.LENGTH_SHORT).show()}
        } else{
            Toast.makeText(this,"Please Fill All The Fields!",Toast.LENGTH_SHORT).show()
        }
    }





}
