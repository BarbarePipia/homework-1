package com.example.homework1

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.telephony.mbms.MbmsErrors
import android.util.Log.d
import android.widget.Button
import kotlinx.android.synthetic.main.activity_main.*
import kotlin.random.Random

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init(){
        randomNumberButton.setOnClickListener {
            val number:Int = randomNumber()

            d("ButtonClicked", "$number")


            randomNumber.text = number.toString()

            if (number > 0 && number % 5 == 0) {Dividable.text = "Yes"}
            else {Dividable.text = "No"}
        }
    }
    private fun randomNumber(): Int {
        val randNumber:Int = (-100..100).random()
        return randNumber
    }

}
